export let keyDownFunc;

export const startTyping = (socket) => {
    const colored = document.getElementById('colored-text');
    const selected = document.getElementById('selected-text');
    const undecorated = document.getElementById('undecorated-text');

    selected.innerText = undecorated.innerText[0];

    undecorated.innerText = undecorated.innerText.slice(1);

    function keyDown({ key }) {
        const notEnteredText = undecorated.innerText;
        const enteredText = colored.innerText;
        const nextValueToEnter = selected.innerText;

        const pressedButtonValue = key === ' ' ? '\xa0' : key;

        if (pressedButtonValue === nextValueToEnter) {
            colored.innerText = enteredText.concat(nextValueToEnter);

            socket.emit('KEY_PRESSED', colored.innerText.length);

            if (notEnteredText.length === 0) {
                selected.innerText = '';
                return;
            } else {
                notEnteredText[0] !== ' '
                    ? (selected.innerText = notEnteredText[0])
                    : (selected.innerText = '\xa0');
            }

            const newTextToEnter = notEnteredText.slice(1);

            undecorated.innerText = newTextToEnter;
        }
    }
    keyDownFunc = keyDown;
    const documentEventListener = document.addEventListener('keydown', keyDown);
};
