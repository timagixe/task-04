import { createElement } from './domHelper.mjs';

const alertMessageBlockElement = document.getElementById('alert-message-block');

export const createAlertMessage = (text) => {
    const messageId = Math.floor(Math.random() * 1000000);

    const alert = createElement({
        tagName: 'div',
        className: 'alert success',
        attributes: {
            id: messageId,
        },
    });

    const alertLogo = createElement({
        tagName: 'div',
        className: 'alert__logo',
    });

    const alertIcon = createElement({
        tagName: 'i',
        className: 'fa fa-android',
        attributes: {
            'aria-hidden': 'true',
        },
    });

    const alertBody = createElement({
        tagName: 'div',
        className: 'alert__body',
    });

    const alertTitle = createElement({
        tagName: 'p',
        className: 'alert__title',
    });

    alertTitle.innerText = 'Jeremy';

    const alertMessage = createElement({
        tagName: 'p',
        className: 'alert__message',
    });

    alertMessage.innerText = text;

    alertLogo.append(alertIcon);
    alertBody.append(alertTitle, alertMessage);
    alert.append(alertLogo, alertBody);
    alertMessageBlockElement.append(alert);

    setTimeout(() => {
        const alertToBeRemoved = document.getElementById(messageId);
        alertToBeRemoved.remove();
    }, 10000);
};
