import {
    hideShowRoom,
    fetchRoomsOnline,
    createRoom,
    fetchRooms,
    fetchUsersInRoom,
} from './handlers/roomHandler.mjs';
import {
    processMonitor,
    progressMonitor,
    stopGame,
} from './handlers/gameHandler.mjs';

import { botMessageMonitor } from './handlers/botMessagesHandler.mjs';

const username = sessionStorage.getItem('username');

const createRoomButtonElement = document.getElementById('create-room-button');

if (!username) {
    window.location.replace('/login');
}

const socket = io('', { query: { username } });

// LOGIN HANDLER
socket.emit('CHECK_USERNAME', username);
socket.on('USERNAME_INVALID', (username) => {
    alert(
        `User ${username} already exists. Please choose a different username`
    );
    sessionStorage.clear();
    window.location.replace('/login');
});

// BOT
botMessageMonitor(socket);

// ROOM HANDLERS
hideShowRoom(socket);
fetchRoomsOnline(socket);
createRoom(socket);
fetchRooms(socket);
fetchUsersInRoom(socket);

// GAME HANDLERS
progressMonitor(socket);
processMonitor(socket);
stopGame(socket);

const onClickCreateRoomButton = () => {
    const roomId = prompt('Write room name');
    if (roomId) {
        socket.emit('CREATE_ROOM', roomId);
    } else {
        alert('Room name cannot be empty');
        return;
    }
};

const createRoomButtonListener = createRoomButtonElement.addEventListener(
    'click',
    onClickCreateRoomButton
);
