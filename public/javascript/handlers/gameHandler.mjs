import { keyDownFunc, startTyping } from '../helpers/updateText.mjs';
import { createElement } from '../helpers/domHelper.mjs';
import { createAlertMessage } from '../helpers/botMessageHelper.mjs';

export const processMonitor = (socket) => {
    let text;
    let timerBeforeGame;

    socket.on('CHANGE_STATUS_SUCCESS', (username) => {
        updateStatusColor(username);
    });

    socket.on('START_GAME', (textId) => {
        showTimerBeforeStart();
        hideButtons();
        timerBeforeGame = document.getElementById('timer-before');
    });
    socket.on('UPDATE_BEFORE_GAME_TIMER', (second) => {
        changeTimerBeforeStartValue(timerBeforeGame, second);
        if (second === 0) {
            hideTimerBeforeStart();
            showGame(text);
            startTyping(socket);
        }
    });
    socket.on('FETCH_TEXT', async (generatedText) => {
        text = await generatedText;
    });
    socket.on('UPDATE_GAME_TIMER', (second) => {
        changeGameTimer(second);
    });
};

const hideButtons = () => {
    const readyButton = document.getElementById('ready-button');
    const backButton = document.getElementById('back-button');
    readyButton.style.display = 'none';
    backButton.style.visibility = 'hidden';
};
const showGame = (text) => {
    const gameBlock = document.getElementById('game-block');
    const gameTimer = createElement({
        tagName: 'div',
        className: 'game-timer',
        attributes: {
            id: 'game-timer',
        },
    });
    const textBlock = createElement({
        tagName: 'div',
        className: '',
        attributes: {
            id: 'text-block',
        },
    });
    const coloredText = createElement({
        tagName: 'span',
        className: '',
        attributes: {
            id: 'colored-text',
        },
    });
    const selectedText = createElement({
        tagName: 'span',
        className: '',
        attributes: {
            id: 'selected-text',
        },
    });
    const undecoratedText = createElement({
        tagName: 'span',
        className: '',
        attributes: {
            id: 'undecorated-text',
        },
    });

    undecoratedText.innerText = text;

    textBlock.append(coloredText, selectedText, undecoratedText);

    gameBlock.append(gameTimer, textBlock);
};
const showTimerBeforeStart = () => {
    const gameBlock = document.getElementById('game-block');
    const timer = createElement({
        tagName: 'div',
        className: 'timer-before',
        attributes: {
            id: 'timer-before',
        },
    });
    gameBlock.append(timer);
};

const changeTimerBeforeStartValue = (timer, value) => {
    timer.innerText = value;
};

const changeGameTimer = (value) => {
    const timer = document.getElementById('game-timer');
    timer.innerText = `${value} seconds left`;
};

const hideTimerBeforeStart = () => {
    const timer = document.getElementById('timer-before');
    timer.remove();
};

const updateStatusColor = (username) => {
    const readyStatus = document.getElementById(`${username}-ready`);
    const classNamesArr = readyStatus.className.split(' ');
    if (classNamesArr.pop() === 'ready') {
        classNamesArr.push('not-ready');
    } else {
        classNamesArr.push('ready');
    }
    readyStatus.className = classNamesArr.join(' ');
};

export const progressMonitor = (socket) => {
    socket.on('UPDATE_USER_PROGRESS', (username, progress) => {
        const progressBar = document.getElementById(`${username}-progress`);
        changeProgressBar(progressBar, progress);
    });
};

const changeProgressBar = (progressBar, progress) => {
    progressBar.style.width = `${progress}%`;
    changeProgressBarColor(progressBar, progress);
};

const changeProgressBarColor = (progressBar, progress) => {
    if (progress === 100) {
        progressBar.style.backgroundColor = 'lightgreen';
    }
    if (progress < 100) {
        progressBar.style.backgroundColor = 'lightgreen';
    }
};

export const stopGame = (socket) => {
    socket.on('STOP_GAME', (results) => {
        document.removeEventListener('keydown', keyDownFunc);
        const arrResults = results.map((resData, i) => {
            if (i === 0) {
                socket.emit('ADD_USER_TO_CHAMPIONS', resData.username);
            }
            return `${i + 1}. ${resData.username} got ${
                resData.progress
            } points!`;
        });
        const stringResults = arrResults.slice(0, 3).join('\n');
        const text = `Game is complete! Results of the run: \n ${stringResults}`;
        createAlertMessage(text);
        setDefaultProperties(socket);
    });
};

const setDefaultProperties = (socket) => {
    hideGameProcess();
    showRoomButtons();
};

const hideGameProcess = () => {
    const gameTimer = document.getElementById('game-timer');
    const textBlock = document.getElementById('text-block');
    gameTimer.remove();
    textBlock.remove();
};

const showRoomButtons = () => {
    const readyButton = document.getElementById('ready-button');
    const backButton = document.getElementById('back-button');
    readyButton.style.display = 'block';
    backButton.style.display = 'block';
    backButton.style.visibility = 'visible';
    readyButton.innerText = 'I AM READY';
};
