import { createAlertMessage } from '../helpers/botMessageHelper.mjs';

const widthToPercentage = (elem) => {
    const pa = document.querySelector('.progress-bar');
    return ((elem.offsetWidth / pa.offsetWidth) * 100).toFixed(0);
};

export const botMessageMonitor = (socket) => {
    socket.on('BOT_WELCOME_USER', (username) => {
        const text = `Hello ${username}. It is Jeremy!`;
        createAlertMessage(text);
    });

    socket.on('BOT_USER_ENTERED_ROOM', (username) => {
        const text = `${username} has joined us. Please welcome him!`;
        createAlertMessage(text);
    });

    socket.on('BOT_USER_ENTERED_ROOM_DIRECT_MESSAGE', (username, roomId) => {
        const text = `Aloha ${username}. You've joined ${roomId} room. Enjoy the game!`;
        createAlertMessage(text);
    });

    socket.on('BOT_USER_LEFT_ROOM', (username) => {
        const text = `${username} has left us :-(`;
        createAlertMessage(text);
    });

    socket.on('BOT_USER_LEFT_ROOM_DIRECT_MESSAGE', (username) => {
        const text = `Hey ${username}, I hope to see you in the game again :-)`;
        createAlertMessage(text);
    });

    socket.on('BOT_START_GAME', (users, secondsBeforeGame) => {
        console.log('123');
        const text = `Game will start in ${secondsBeforeGame} seconds. Today's participants are: ${users.join(
            ', '
        )}. GOOD LUCK!`;
        createAlertMessage(text);
    });

    socket.on('BOT_USER_ON_FINISH_LINE', (username) => {
        const text = `User ${username} is already at the finish line. Hurry up!`;
        createAlertMessage(text);
    });

    socket.on('BOT_UPDATE_USERS_ABOUT_CURRENT_RESULTS', () => {
        const test = document.querySelectorAll('.progress');
        const res = [`So far the results are: \n`];
        const array = Array.prototype.slice.call(test);

        for (let item of array) {
            const username = item.id.split('-')[0];
            const points = widthToPercentage(item);
            res.push(`${username} scored ${points} points \n`);
        }

        createAlertMessage(res.join(''));
    });
};
