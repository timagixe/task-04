import { MAXIMUM_USERS_FOR_ONE_ROOM } from '../config';
import {
    isGameReady,
    letsStartGame,
    letsStopGameProcess,
} from './gameProcessHandler';
import { isGameDone } from './gameProgressHandler';
import {
    addUserToUsersInRoom,
    removeUserFromUsersInRoom,
} from '../bot/botHandler';

export const mapOfRooms = new Map();
export const mapStatusToRooms = new Map();
export const mapTimersToRooms = new Map();

// RETRIEVES ROOM (TAKEN FROM COUNTER PROJECT)
export const getCurrentRoomId = (socket) =>
    Object.keys(socket.rooms).find((roomId) => mapOfRooms.has(roomId));

// HANDLES ROOMS STATUSES
export const updateRoomStatus = (roomId, status) => {
    mapStatusToRooms.set(roomId, status);
};

// RETRIEVES ROOM STATUS
export const fetchRoomStatus = (roomId) => {
    const status = mapStatusToRooms.get(roomId);
    if (!status) return 'created';
    return mapStatusToRooms.get(roomId);
};

// ADD TIMER TO THE ROOM
export const updateRoomWithTimer = (roomId, timer) => {
    const timers = mapTimersToRooms.get(roomId);
    if (!timers) {
        const newTimers = [];
        newTimers.push(timer);
        mapTimersToRooms.set(roomId, newTimers);
    } else {
        timers.push(timer);
    }
};

// REMOVE TIMER FROM THE ROOM
export const removeRoomTimers = (roomId) => {
    const timers = mapTimersToRooms.get(roomId);
    if (timers) {
        timers.forEach((timer) => clearTimeout(timer));
        mapTimersToRooms.set(roomId, []);
    }
};

// RETRIEVES USERS FROM ROOM
export const getUserFromRoom = (socket) => {
    const roomId = getCurrentRoomId(socket);
    const usersInRoom = mapOfRooms.get(roomId);

    if (roomId) {
        let neededUser;
        usersInRoom.forEach((user) => {
            if (user.id === socket.id) {
                neededUser = user;
                return;
            }
        });
        return neededUser;
    } else {
        return undefined;
    }
};

// CHANGE ROOM FOR USER AND DROP DATA
export const setRoom = (roomId) => {
    const room = mapOfRooms.get(roomId);
    room.forEach((user) => {
        user.progress = 0;
        user.ready = false;
    });
};

// FETCHES ALL THE ROOMS
export const fetchRooms = (socket) => {
    socket.on('FETCH_ROOMS', () => {
        const allRooms = [];
        mapOfRooms.forEach((roomUsers, roomId) => {
            const mapStatusToRooms = fetchRoomStatus(roomId);
            if (
                roomUsers.size < MAXIMUM_USERS_FOR_ONE_ROOM &&
                mapStatusToRooms !== 'started'
            )
                allRooms.push(roomId);
        });
        socket.emit('FETCH_ROOMS_SUCCESS', allRooms);
    });
};

// CREATES ROOM ON USER REQUEST
export const createRoomOnRequest = (io, socket) => {
    socket.on('CREATE_ROOM', (roomId) => {
        if (mapOfRooms.has(roomId)) {
            // ROOM EXISTS - THROW ERRORS
            socket.emit('CREATE_ROOM_FAILED', roomId);
        } else {
            // IF NO SUCH ROOM - CREATE ROOM AND CONNECT USER TO IT
            createRoom(roomId);
            io.emit('UPDATE_ROOMS_LIST', roomId);
            socket.emit('CREATE_ROOM_SUCCESS', roomId);
        }
    });
};

// CREATE AND UPDATE MAP OF ROOMS WITH NEW ROOM
export const createRoom = (roomId) => {
    const set = new Set();
    mapOfRooms.set(roomId, set);
    mapStatusToRooms.set(roomId, 'created');
};

// UPDATE ONLINE NUMBERS OF EXISTING ROOMS
export const fetchRoomsOnline = (io, socket) => {
    socket.on('FETCH_ROOMS_ONLINE', (roomId) => {
        const currentRoom = io.sockets.adapter.rooms[roomId];
        const roomOnline = currentRoom.length;

        socket.emit('FETCH_ROOMS_ONLINE_SUCCESS', roomId, roomOnline);
    });
};

// HANDLE USER ENTER THE ROOM
export const enterRoom = (io, socket) => {
    socket.on('ENTER_ROOM', (roomId) => {
        joinUser(io, socket, roomId);

        const username = socket.handshake.query.username;

        socket.emit('BOT_USER_ENTERED_ROOM_DIRECT_MESSAGE', username, roomId);
        addUserToUsersInRoom(username);
        socket.to(roomId).emit('BOT_USER_ENTERED_ROOM', username);
    });
};

// MAP USER TO ROOM
export const joinUser = (io, socket, roomId) => {
    const username = socket.handshake.query.username;
    const usersInRoom = mapOfRooms.get(roomId);
    const userObject = {
        id: socket.id,
        username: username,
        progress: 0,
        ready: false,
    };

    usersInRoom.add(userObject);
    socket.join(roomId);
    mapOfRooms.set(roomId, usersInRoom);

    io.emit('FETCH_ROOMS_ONLINE_SUCCESS', roomId, usersInRoom.size);

    // IF ROOM IS FULL - HIDE IT
    if (MAXIMUM_USERS_FOR_ONE_ROOM === usersInRoom.size) {
        io.emit('HIDE_ROOM_IN_LOBBY', roomId);
    }

    socket.emit('FETCH_USERS_IN_ROOM', fetchUsersInRoom(roomId), roomId);
    socket.to(roomId).emit('UPDATE_ROOM_WITH_USER', username);
};

// RETRIEVES USERS IN ROOM ID
export const fetchUsersInRoom = (roomId) => {
    const usersInRoom = mapOfRooms.get(roomId);
    const usernames = [];
    usersInRoom.forEach((user) => {
        usernames.push(user.username);
    });
    return usernames;
};

// HANDLE USER LEFT ROOM
export const leaveRoom = (io, socket) => {
    socket.on('USER_LEFT_ROOM', () => {
        userLeftTheGame(io, socket);
    });
};

// REMOVE USER AND USER DATA
export const userLeftTheGame = (io, socket) => {
    const roomId = getCurrentRoomId(socket);

    if (roomId) {
        const username = socket.handshake.query.username;
        const usersInRoom = mapOfRooms.get(roomId);
        const userToDelete = getUserFromRoom(socket);

        usersInRoom.delete(userToDelete);
        socket.to(roomId).emit('DELETE_USER_FROM_ROOM', username);

        socket.emit('BOT_USER_LEFT_ROOM_DIRECT_MESSAGE', username);
        removeUserFromUsersInRoom(username);
        socket.to(roomId).emit('BOT_USER_LEFT_ROOM', username);

        if (fetchRoomStatus(roomId) !== 'started') {
            if (isGameReady(socket)) letsStartGame(io, socket, roomId);
        }
        socket.leave(roomId);
        mapOfRooms.set(roomId, usersInRoom);

        if (isGameDone(socket)) {
            letsStopGameProcess(io, socket);
        }

        // DELETE ROOM IF THERE ARE NO USERS
        if (usersInRoom.size === 0) {
            deleteRoom(io, roomId);
            removeRoomTimers(roomId);
            return;
        }

        // SHOW ROOM IF IT IS NOT FULL AND GAME IS NOT RUNNING
        if (
            usersInRoom.size < MAXIMUM_USERS_FOR_ONE_ROOM &&
            fetchRoomStatus(roomId) === 'created'
        ) {
            io.emit('SHOW_ROOM_ON_LOBBY', roomId);
        }

        io.emit('FETCH_ROOMS_ONLINE_SUCCESS', roomId, usersInRoom.size);
    }
};

// HANDLE ROOM DELETION
const deleteRoom = (io, roomId) => {
    mapOfRooms.delete(roomId);
    io.emit('DELETE_ROOM', roomId);
};
