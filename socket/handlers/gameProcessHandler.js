import {
    SECONDS_TIMER_BEFORE_START_GAME,
    SECONDS_FOR_GAME,
    MAXIMUM_USERS_FOR_ONE_ROOM,
} from '../config';

import { setTextLength } from './gameProgressHandler';
import {
    removeRoomTimers,
    setRoom,
    getUserFromRoom,
    getCurrentRoomId,
    mapOfRooms,
    updateRoomStatus,
    updateRoomWithTimer,
} from './roomHandler.js';
import {
    mergeResults,
    fetchRoomResults,
    dropResults,
} from './gameProgressHandler';
import fetch from 'node-fetch';
import { getUsersInRoom, championsList } from '../bot/botHandler';

// PARTIAL APPLICATION
class CreateUri {
    constructor(protocol) {
        this.protocol = protocol;
    }

    buildUri(domain, path) {
        return `${this.protocol}://${domain}/${path}`;
    }
}

// FOR PROXY
class GetTextAPI {
    async getText() {
        const httpsCreator = new CreateUri('https');
        const quotableRandom = httpsCreator.buildUri(
            'api.quotable.io',
            'random'
        );

        const res = await fetch(quotableRandom);
        const response = await res.json();
        const data = response.content;

        return data;
    }
}

// PROXY
class GetTextAPIProxy {
    constructor() {
        this.api = new GetTextAPI();
        this.cache = [];
    }

    async getText(id) {
        if (this.cache[id] === undefined) {
            console.log('taken from api');
            const value = await this.api.getText();
            this.cache.push(value);
            console.log('new cache is =>', this.cache);
            return value;
        }
        console.log('taken from proxy');
        return this.cache[id];
    }
}

const getTextAPIProxy = new GetTextAPIProxy();

const ONE_SECOND_IN_MILLISECONDS = 1000;

// UPDATE USER STATUS
const updateStatus = (socket) => {
    const user = getUserFromRoom(socket);
    if (user) {
        user.ready = !user.ready;
    }
};

// CHECKS IF GAME IS READY
export const isGameReady = (socket) => {
    const roomId = getCurrentRoomId(socket);
    const room = mapOfRooms.get(roomId);
    let gameIsReady = true;
    if (room) {
        room.forEach((user) => {
            if (!user.ready) gameIsReady = false;
        });
        return gameIsReady;
    } else {
        return false;
    }
};

async function getText(io, roomId) {
    const randomId = Math.floor(Math.random() * 5);
    const data = await getTextAPIProxy.getText(randomId);

    setTextLength(data.length);
    io.in(roomId).emit('FETCH_TEXT', data);
    return data;
}

// STARTS THE GAME
export const letsStartGame = (io, socket, roomId) => {
    console.log('1231');
    updateRoomStatus(roomId, 'started');
    io.emit('HIDE_ROOM_IN_LOBBY', roomId);
    io.in(roomId).emit('START_GAME');
    const arrayOfUsers = getUsersInRoom();
    io.in(roomId).emit(
        'BOT_START_GAME',
        arrayOfUsers,
        SECONDS_TIMER_BEFORE_START_GAME
    );
    getText(io, roomId);
    startTimerBeforeGame(io, socket, roomId);
};

// STARTS TIMER BEFORE GAME
const startTimerBeforeGame = (io, socket, roomId) => {
    let seconds = SECONDS_TIMER_BEFORE_START_GAME;
    let timerId = setTimeout(function timeDecrement() {
        io.in(roomId).emit('UPDATE_BEFORE_GAME_TIMER', seconds);
        if (seconds === 0) {
            letsStartGameProcess(io, socket, roomId);
            return;
        }
        seconds -= 1;
        timerId = setTimeout(timeDecrement, ONE_SECOND_IN_MILLISECONDS);
        updateRoomWithTimer(roomId, timerId);
    }, ONE_SECOND_IN_MILLISECONDS);
    updateRoomWithTimer(roomId, timerId);
};

// STARTS THE GAME PROCESS
export const letsStartGameProcess = (io, socket, roomId) => {
    let seconds = SECONDS_FOR_GAME;
    let updateResultsStatusAtSeconds = SECONDS_FOR_GAME - 10;
    let timerId = setTimeout(function timeDecrement() {
        if (updateResultsStatusAtSeconds === seconds) {
            updateResultsStatusAtSeconds -= 10;
            io.in(roomId).emit('BOT_UPDATE_USERS_ABOUT_CURRENT_RESULTS');
        }
        io.in(roomId).emit('UPDATE_GAME_TIMER', seconds);
        if (seconds === 0) {
            letsStopGameProcess(io, socket);
            return;
        }
        seconds -= 1;
        timerId = setTimeout(timeDecrement, ONE_SECOND_IN_MILLISECONDS);
        updateRoomWithTimer(roomId, timerId);
    }, ONE_SECOND_IN_MILLISECONDS);
    updateRoomWithTimer(roomId, timerId);
};

// PROCESSES THE GAME
export const gameProcess = (io, socket) => {
    socket.on('CHANGE_STATUS', () => {
        const roomId = getCurrentRoomId(socket);
        const username = socket.handshake.query.username;
        io.in(roomId).emit('CHANGE_STATUS_SUCCESS', username);
        updateStatus(socket);
        if (isGameReady(socket)) letsStartGame(io, socket, roomId);
    });

    socket.on('TRIGGER_CHECK_STATUS_FOR_USER', (username) => {
        const roomId = getCurrentRoomId(socket);
        const room = mapOfRooms.get(roomId);
        room.forEach((user) => {
            if (user.username === username) {
                socket.emit('CHECK_STATUS', username, user.ready);
                return;
            }
        });
    });
};

// HANDLES STOPPING THE GAME
export const letsStopGameProcess = (io, socket, roomName) => {
    let roomId;
    if (!roomName) {
        roomId = getCurrentRoomId(socket);
    } else {
        roomId = roomName;
    }
    if (roomId) {
        removeRoomTimers(roomId);
        mergeResults(roomId);
        const results = fetchRoomResults(roomId);
        dropResults(roomId);
        io.to(roomId).emit('STOP_GAME', results);
        socket.on('ADD_USER_TO_CHAMPIONS', (username) => {
            championsList.push(username);
        });
        updateRoomStatus(roomId, 'created');
        setRoom(roomId);
        const room = mapOfRooms.get(roomId);
        room.forEach((user) => {
            io.in(roomId).emit('CHECK_STATUS', user.username, user.ready);
            io.in(roomId).emit('UPDATE_USER_PROGRESS', user.username, 0);
        });
        if (room.size < MAXIMUM_USERS_FOR_ONE_ROOM)
            io.emit('SHOW_ROOM_ON_LOBBY', roomId);
    }
};
