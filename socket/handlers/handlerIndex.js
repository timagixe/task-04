import {
    fetchRoomsOnline,
    enterRoom,
    createRoomOnRequest,
    fetchRooms,
    leaveRoom,
    userLeftTheGame,
} from './roomHandler';
import { monitorUsersProgress } from './gameProgressHandler';
import { gameProcess, letsStopGameProcess } from './gameProcessHandler';
import { Champion } from '../bot/botHandler';

const activeUsers = new Map();

export const mainHandler = (io, socket) => {
    // HANDLES LOG IN
    socket.on('CHECK_USERNAME', (username) => {
        if (activeUsers.has(username)) {
            socket.emit('USERNAME_INVALID', username);
        } else {
            activeUsers.set(username, socket.id);
            const usernameFacade = new Champion();
            socket.emit('BOT_WELCOME_USER', usernameFacade.applyFor(username));
        }
    });

    // ROOM HANDLERS
    fetchRoomsOnline(io, socket);
    enterRoom(io, socket);
    createRoomOnRequest(io, socket);
    fetchRooms(socket);
    leaveRoom(io, socket);

    // GAME HANDLERS
    monitorUsersProgress(io, socket);
    gameProcess(io, socket);
    letsStopGameProcess(io, socket);
};

export const disconnectHandler = (io, socket, username) => {
    // DELETE USER FROM ACTIVES
    if (activeUsers.get(username) === socket.id) {
        activeUsers.delete(username);
    }

    userLeftTheGame(io, socket);
};
