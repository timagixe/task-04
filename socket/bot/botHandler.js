export let usersInRoomForBot = [];
export let championsList = [];
export const resultsForBot = new Map();

export const addUserToUsersInRoom = (username) => {
    usersInRoomForBot.push(username);
};

export const removeUserFromUsersInRoom = (username) => {
    const newUsersInRoom = usersInRoomForBot.filter(
        (user) => user !== username
    );
    usersInRoomForBot = newUsersInRoom;
};

export const getUsersInRoom = () => {
    return usersInRoomForBot;
};

// FACADE
export class Champion {
    constructor(name) {
        this.name = name;
    }

    applyFor(name) {
        let result;

        if (new Victories().confirm(name)) {
            result = `the Champ ${name}`;
        } else {
            result = `newcomer ${name}`;
        }

        return result;
    }
}

export class Victories {
    confirm(name) {
        return championsList.includes(name) ? true : false;
    }
}
