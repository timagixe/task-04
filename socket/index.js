import { mainHandler, disconnectHandler } from './handlers/handlerIndex';

export default (io) => {
    io.on('connection', (socket) => {
        const username = socket.handshake.query.username;

        mainHandler(io, socket);

        // HANDLE DISCONNECT
        socket.on('disconnecting', () => {
            disconnectHandler(io, socket, username);
        });
    });
};
